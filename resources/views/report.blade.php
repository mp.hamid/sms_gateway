<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SMS Gateway</title>

    <!-- Fonts -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css"
          integrity="sha256-3sPp8BkKUE7QyPSl6VfBByBroQbKxKG7tsusY2mhbVY=" crossorigin="anonymous"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <style>
        body {
            background: #f5f5f5;
            margin-top: 20px;
            direction: rtl;
        }

        /* ===== Career ===== */
        .career-form {
            background-color: #4e63d7;
            border-radius: 5px;
            padding: 0 16px;
        }

        .career-form .form-control {
            background-color: rgba(255, 255, 255, 0.2);
            border: 0;
            padding: 12px 15px;
            color: #fff;
        }

        .career-form .form-control::-webkit-input-placeholder {
            /* Chrome/Opera/Safari */
            color: #fff;
        }

        .career-form .form-control::-moz-placeholder {
            /* Firefox 19+ */
            color: #fff;
        }

        .career-form .form-control:-ms-input-placeholder {
            /* IE 10+ */
            color: #fff;
        }

        .career-form .form-control:-moz-placeholder {
            /* Firefox 18- */
            color: #fff;
        }

        .career-form .custom-select {
            background-color: rgba(255, 255, 255, 0.2);
            border: 0;
            padding: 12px 15px;
            color: #fff;
            width: 100%;
            border-radius: 5px;
            text-align: left;
            height: auto;
            background-image: none;
        }

        .career-form .custom-select:focus {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .career-form .select-container {
            position: relative;
        }

        .career-form .select-container:before {
            position: absolute;
            right: 15px;
            top: calc(50% - 14px);
            font-size: 18px;
            color: #ffffff;
            content: '\F2F9';
            font-family: "Material-Design-Iconic-Font";
        }

        .filter-result .job-box {
            -webkit-box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
            box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
            border-radius: 10px;
            padding: 10px 35px;
        }

        ul {
            list-style: none;
        }

        .list-disk li {
            list-style: none;
            margin-bottom: 12px;
        }

        .list-disk li:last-child {
            margin-bottom: 0;
        }

        .job-box .img-holder {
            height: 65px;
            width: 65px;
            background-color: #4e63d7;
            background-image: -webkit-gradient(linear, left top, right top, from(rgba(78, 99, 215, 0.9)), to(#5a85dd));
            background-image: linear-gradient(to right, rgba(78, 99, 215, 0.9) 0%, #5a85dd 100%);
            font-family: "Open Sans", sans-serif;
            color: #fff;
            font-size: 22px;
            font-weight: 700;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            border-radius: 65px;
        }

        .career-title {
            background-color: #4e63d7;
            color: #fff;
            padding: 15px;
            text-align: center;
            border-radius: 10px 10px 0 0;
            background-image: -webkit-gradient(linear, left top, right top, from(rgba(78, 99, 215, 0.9)), to(#5a85dd));
            background-image: linear-gradient(to right, rgba(78, 99, 215, 0.9) 0%, #5a85dd 100%);
        }

        .job-overview {
            -webkit-box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
            box-shadow: 0 0 35px 0 rgba(130, 130, 130, 0.2);
            border-radius: 10px;
        }

        @media (min-width: 992px) {
            .job-overview {
                position: -webkit-sticky;
                position: sticky;
                top: 70px;
            }
        }

        .job-overview .job-detail ul {
            margin-bottom: 28px;
        }

        .job-overview .job-detail ul li {
            opacity: 0.75;
            font-weight: 600;
            margin-bottom: 15px;
        }

        .job-overview .job-detail ul li i {
            font-size: 20px;
            position: relative;
            top: 1px;
        }

        .job-overview .overview-bottom,
        .job-overview .overview-top {
            padding: 35px;
        }

        .job-content ul li {
            font-weight: 600;
            opacity: 0.75;
            border-bottom: 1px solid #ccc;
            padding: 10px 5px;
        }

        @media (min-width: 768px) {
            .job-content ul li {
                border-bottom: 0;
                padding: 0;
            }
        }

        .job-content ul li i {
            font-size: 20px;
            position: relative;
            top: 1px;
        }

        .mb-30 {
            margin-bottom: 30px;
        }

        table td {
            direction: ltr
        }

        .status {
            display: flex;
        }

        .status div:first-child {
            margin-right: 0;
        }

        .status > div {
            margin: 0 7px;
            border-radius: 5px;
            border: 1px #b4b4b4 solid;
            padding: 5px;
            position: relative;
            width: 120px;
            text-align: center;
        }

        .status div .title {
            font-size: 13px;
            font-weight: bold;
        }

        .status div .date {
            direction: ltr;
            font-size: 11px;
            color: #888888;
        }
        .response pre{
            font-size: 12px;
            direction: ltr;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg-10 mx-auto mb-4">
            <div class="section-title text-center ">
                <h3 class="top-c-sep">Sms Gateway</h3>
                <p>گزارش ارسال های انجام شده به همراه لیست تغییر وضعیت های اتفاق افتاده</p>
                <p>توجه کنید که در صورتی که ارسال با خطا مواجه شود خطای صادر شده از ارسال کننده پیامک نمایش داده
                    میشود.</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-10 mx-auto">
            <div class="career-search mb-60">


                <div class="filter-result">

                    @foreach($reports as $report)

                        <div class="job-box align-items-center justify-content-between mb-30">
                            <div>
                                <h5 class="text-center text-md-right">{{$report->id.' - '.$report->message}}</h5>
                            </div>
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 150px"><i class="zmdi zmdi-flash mr-2"></i> {{$report->provider}}
                                    </td>
                                    <td style="width: 200px">{{$report->sender}}</td>
                                    <td>
                                        <ul>
                                            @foreach($report->recipients as $receptor)
                                                <li><i class="zmdi zmdi-assignment-account mr-2"></i>{{$receptor}}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td><i class="zmdi zmdi-time mr-2"></i> {{$report->created_at}}</td>
                                </tr>
                            </table>
                            <div class="status">
                                @foreach($report->status as $status)
                                    <div>
                                        <div class="title">{{__('status.'.$status->status)}}</div>
                                        <div class="date">{{$status->created_at}}</div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="response">
                                @foreach($report->status as $status)
                                    @if(!empty($status->response))
                                    <pre>{{print_r(json_decode($status->response->response,true))}}</pre>
                                    @endif
                                @endforeach

                            </div>
                        </div>
                    @endforeach

                </div>
            </div>

            {{$reports->onEachSide(5)->links()}}
        </div>
    </div>

</div>
</body>
</html>
