<?php

use App\Models\ReportStatus;

return[
  ReportStatus::STATUS_QUEUED => 'در صف ارسال',
  ReportStatus::STATUS_SENDING => 'شروع ارسال',
  ReportStatus::STATUS_SUCCESS => 'ارسال موفق',
  ReportStatus::STATUS_FAILED => 'ارسال نا موفق',
];
