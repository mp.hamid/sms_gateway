<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_failed_responses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('report_id');
            $table->foreign('report_id')
                ->references('id')
                ->on('reports')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedBigInteger('report_status_id');
            $table->foreign('report_status_id')
                ->references('id')
                ->on('report_statuses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->text('response');
            $this->timestamp('created_at', 0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_failed_responses');
    }
};
