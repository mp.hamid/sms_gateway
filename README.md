## SMS Gateway

The purpose of this project is to design and implement a service for sending and managing SMS provider with Laravel.

## How to run

Run the following command in the terminal and wait for all images to be downloaded from Docker Hub

```shell
docker-compose up -d
```

**Note :** this docker file is made for the developer environment and includes the Xdebug PHP extension

To launch the project, you must execute the following commands in the smsGateway-app shell section

Run the following command to access the shell

```shell
docker exec -it smsGateway-app /bin/bash
```

Execute the following commands to install the required packages and initialize the database

```shell
composer install
php artisan migrate
```

## How to implement new sms provider
All you have to do is create a new class in the `App\SmsSevice\Providers` namespace and extend from the `App\SmsSevice\SmsProvider` class

## General Enviroments

`DEFAULT_PROVIDER`

## Kavenegar Enviroments

`KAVENEGAR_API_KEY`

`KAVENEGAR_DEFAULT_SENDER_NUMBER`

## Magfa Enviroments

`MAGFA_USER_NAME`

`MAGFA_PASSWORD`

`MAGFA_DEFAULT_SENDER_NUMBER`
