<?php

use App\Http\Controllers\Api\V1\ReportController;
use App\Http\Controllers\Api\V1\SendController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('send', [SendController::class, 'send']);

Route::get('report', [ReportController::class, 'report']);


