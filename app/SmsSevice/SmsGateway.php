<?php

namespace App\SmsSevice;

use App\Exceptions\SmsGatewayException;
use App\Models\Report;
use App\Models\ReportFailedResponse;
use App\Models\ReportStatus;

/**
 * @method $this setSenderNumber() setSenderNumber(string $senderNumber) set sender number
 * @method $this setReceptor() setReceptor(string $receptor) set receptor
 * @method $this setRecipients() setRecipients(array $recipients) set receptor batch with array
 * @method $this setMessage() setMessage(string $message) set message
 */
class SmsGateway
{
    private SmsProvider $provider;
    private string $providerName;
    private int $reportId;

    /**
     * @param string $providerName
     * @return $this
     * @throws SmsGatewayException
     */
    public function setProvider(string $providerName): static
    {
        $this->providerName = toCamelCase($providerName);
        $providerNameSpace = 'App\Factories\Providers\\' . $this->providerName;
        if (!class_exists($providerNameSpace)) {
            throw new SmsGatewayException(SmsGatewayException::WRONG_PROVIDER_NAME);
        }
        if (empty($this->provider) || !($this->provider instanceof $providerNameSpace)) {
            $this->provider = new $providerNameSpace();
        }
        return $this;
    }

    /**
     * @throws SmsGatewayException
     */
    public function storeReport(): bool
    {
        $report = Report::create([
            'provider' => $this->providerName,
            'sender' => $this->provider->getSenderNumber(),
            'recipients' => json_encode($this->provider->getRecipients()),
            'message' => $this->provider->getMessage()
        ]);
        $this->reportId = $report->id;
        $this->storeStatus(ReportStatus::STATUS_QUEUED);
        return true;
    }

    /**
     * @param $status
     * @return int
     * @throws SmsGatewayException
     */
    private function storeStatus($status): int
    {
        if (empty($this->reportId)) {
            return 0;
        }
        $reportStatus = ReportStatus::create(['report_id' => $this->reportId, 'status' => $status]);
        return $reportStatus->id;
    }

    /**
     * @throws SmsGatewayException
     */
    public function send(): bool
    {
        $this->storeStatus(ReportStatus::STATUS_SENDING);
        $result = $this->provider->send();
        if (!$result) {
            if (!empty($this->reportId)) {
                $reportStatusId = $this->storeStatus(ReportStatus::STATUS_FAILED);
                $httpErrorResponse = $this->provider->getHttpErrorMessage();
                ReportFailedResponse::create([
                    'report_id' => $this->reportId,
                    'report_status_id' => $reportStatusId,
                    'response' => is_array($httpErrorResponse) ? json_encode($httpErrorResponse) : $httpErrorResponse
                ]);
            }
            return false;
        }
        $this->storeStatus(ReportStatus::STATUS_SUCCESS);

        return $result;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws SmsGatewayException
     */
    public function __call(string $name, array $arguments)
    {
        if (!isset($this->provider)) {
            $this->setProvider(config('sms.default_provider'));
        }
        if (!method_exists($this->provider, $name)) {
            throw new SmsGatewayException(SmsGatewayException::CALL_UNDEFINED_METHOD);
        }
        return $this->provider->$name(...$arguments);
    }
}
