<?php

namespace App\SmsSevice\Providers;


use App\SmsSevice\SmsProvider;
use Illuminate\Http\Client\Response;

class Magfa extends SmsProvider
{
    protected array $setting;
    protected string $apiPath = 'https://sms.magfa.com/api/http/sms/v2/send';

    public function __construct()
    {
        $this->setting = config('sms.providers.magfa');
    }

    /**
     * @inheritDoc
     */
    protected function getDefaultSenderNumber(): string
    {
        return config('sms.providers.magfa.default_number');
    }

    /**
     * @inheritDoc
     */
    public function send(): bool
    {
        $params = [
            "senders" => [$this->getSenderNumber()],
            "recipients" => $this->getRecipients(),
            "messages" => [$this->getMessage()],
        ];
        $headers = [
            'Accept: application/json',
            'Content-Type: application/json',
            'charset: utf-8',
            'Authorization: Basic ' . config('sms.providers.Magfa.api_key'),
        ];

        return $this->executeRequest($this->apiPath, $params, $headers);
    }

    /**
     * @inheritDoc
     */
    protected function sendErrorHandler(Response $response)
    {
        $responseJson = $response->json();
        if (empty($responseJson)) {
            return '';
        }
        if ($responseJson['status'] != 0) {
            return ['message' => $this->get_error_message($responseJson['status'])];
        }

        return $responseJson;
    }

    /**
     * @param int $status
     * @return string|void
     */
    private function get_error_message(int $status)
    {
        switch ($status) {
            case 1:
                return 'شماره گيرنده نادرست است';
            case 2:
                return 'شماره فرستنده نادرست است';
            case 3:
                return 'پارامتر encoding نامعتبراست. (بررسی صحت و هم‌خوانی متن پيامک با encoding انتخابی)';
            case 4:
                return 'پارامتر mclass نامعتبر است';
            case 6:
                return 'پارامتر UDH نامعتبر است';
            case 13:
                return 'محتويات پيامک (تركيب UDH و متن) خالی است. (بررسی دوباره‌ی متن پيامک و پارامتر UDH)';
            case 14:
                return 'مانده اعتبار ريالی مورد نياز برای ارسال پیامک کافی نيست';
            case 15:
                return 'سرور در هنگام ارسال پيام مشغول برطرف نمودن ايراد داخلی بوده است. (ارسال مجدد درخواست)';
            case 16:
                return 'حساب غيرفعال است. (تماس با واحد فروش سيستم‌های ارتباطی)';
            case 17:
                return 'حساب منقضی شده است. (تماس با واحد فروش سيستم‌های ارتباطی)';
            case 18:
                return 'نام كاربری و يا كلمه عبور نامعتبر است. (بررسی مجدد نام كاربری و كلمه عبور)';
            case 19:
                return 'درخواست معتبر نيست. (تركيب نام كاربری، رمز عبور و دامنه اشتباه است. تماس با واحد فروش برای دريافت كلمه عبور جديد)';
            case 20:
                return 'شماره فرستنده به حساب تعلق ندارد';
            case 22:
                return 'اين سرويس برای حساب فعال نشده است';
            case 23:
                return 'در حال حاضر امکان پردازش درخواست جديد وجود ندارد، لطفا دوباره سعی كنيد. (ارسال مجدد درخواست)';
            case 24:
                return 'شناسه پيامک معتبر نيست. (ممكن است شناسه پيامک اشتباه و يا متعلق به پيامكی باشد كه بيش از يک روز از ارسال آن گذشته)';
            case 25:
                return 'نام متد درخواستی معتبر نيست. (بررسی نگارش نام متد با توجه به بخش متدها در اين راهنما)';
            case 27:
                return 'شماره گيرنده در ليست سياه اپراتور قرار دارد. (ارسال پيامک‌های تبليغاتی برای اين شماره امكان‌پذير نيست)';
            case 28:
                return 'شماره گیرنده، بر اساس پیش‌شماره در حال حاضر در مگفا مسدود است';
            case 29:
                return 'آدرس IP مبدا، اجازه دسترسی به این سرویس را ندارد';
            case 30:
                return 'تعداد بخش‌های پیامک بیش از حد مجاز استاندارد (۲۶۵ عدد) است';
            case 101:
                return 'طول آرايه پارامتر messageBodies با طول آرايه گيرندگان تطابق ندارد';
            case 102:
                return 'طول آرايه پارامتر messageClass با طول آرايه گيرندگان تطابق ندارد';
            case 103:
                return 'طول آرايه پارامتر senderNumbers با طول آرايه گيرندگان تطابق ندارد';
            case 104:
                return 'طول آرايه پارامتر udhs با طول آرايه گيرندگان تطابق ندارد';
            case 105:
                return 'طول آرايه پارامتر priorities با طول آرايه گيرندگان تطابق ندارد';
            case 106:
                return 'آرايه‌ی گيرندگان خالی است';
            case 107:
                return 'طول آرايه پارامتر گيرندگان بيشتر از طول مجاز است';
            case 108:
                return 'آرايه‌ی فرستندگان خالی است';
            case 109:
                return 'طول آرايه پارامتر encoding با طول آرايه گيرندگان تطابق ندارد';
            case 110:
                return 'طول آرايه پارامتر checkingMessageIds با طول آرايه گيرندگان تطابق ندارد';
        }
    }

}
