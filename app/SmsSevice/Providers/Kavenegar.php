<?php

namespace App\SmsSevice\Providers;

use App\SmsSevice\SmsProvider;

class Kavenegar extends SmsProvider
{

    protected string $apiKey;
    private string $apiPath;

    public function __construct()
    {
        $this->apiKey = config('sms.providers.kavenegar.api_key');
        $this->apiPath = sprintf("https://api.kavenegar.com/v1/%s/sms/send.json/", $this->apiKey);
    }

    /**
     * @inheritDoc
     */
    protected function getDefaultSenderNumber(): string
    {
        return config('sms.providers.kavenegar.default_number');
    }

    /**
     * @inheritDoc
     */
    public function send(): bool
    {
        $params = [
            "receptor" => implode(",", $this->getRecipients()),
            "sender" => $this->getSenderNumber(),
            "message" => $this->getMessage(),
            "date" => null,
            "type" => null,
            "localid" => null
        ];
        $headers = [
            'Accept: application/json',
            'Content-Type: application/x-www-form-urlencoded',
            'charset: utf-8'
        ];

        return $this->executeRequest($this->apiPath, $params, $headers);
    }

    /**
     * @inheritDoc
     */
    protected function sendErrorHandler($response)
    {
        $responseJson = $response->json();
        if (empty($responseJson)) {
            return '';
        }
        return $responseJson['return']['messages'] ?? $responseJson;
    }
}
