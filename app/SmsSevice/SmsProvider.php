<?php

namespace App\SmsSevice;

use App\Exceptions\SmsGatewayException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

abstract class SmsProvider
{
    private string $senderNumber;
    private array $recipients = [];
    private string $message = '';
    private $httpErrorMessage;


    /**
     * @return bool
     * @throws SmsGatewayException
     */
    abstract public function send(): bool;

    /**
     * @return string
     */
    abstract protected function getDefaultSenderNumber(): string;

    /**
     * @param Response $response
     */
    abstract protected function sendErrorHandler(Response $response);

    public function executeRequest(string $path, array $params, array $headers): bool
    {
        $response = Http::withHeaders($headers)->post($path, $params);
        if ($response->status() != 200) {
            $this->httpErrorMessage = $this->sendErrorHandler($response);
            return false;
        }

        return true;
    }

    public function getHttpErrorMessage()
    {
        return $this->httpErrorMessage;
    }

    public function setSenderNumber($senderNumber): static
    {
        $this->senderNumber = $senderNumber;

        return $this;
    }

    public function setMessage(string $message): static
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @param string $receptor
     * @return $this
     * @throws SmsGatewayException
     */
    public function setReceptor(string $receptor): static
    {
        if (!checkMobileNumber($receptor)) {
            throw new SmsGatewayException(SmsGatewayException::INVALID_MOBILE_NUMBER);
        }
        $this->recipients = array_merge($this->recipients, [$receptor]);

        return $this;
    }

    /**
     * @param array $recipients
     * @return $this
     * @throws SmsGatewayException
     */
    public function setRecipients(array $recipients): static
    {
        $filterArray = array_filter($recipients, 'checkMobileNumber');
        if (count($filterArray) < count($recipients)) {
            throw new SmsGatewayException(SmsGatewayException::INVALID_MOBILE_NUMBER);
        }
        $this->recipients = array_merge($this->recipients, $recipients);

        return $this;
    }

    /**
     * @return string
     * @throws SmsGatewayException
     */
    public function getSenderNumber(): string
    {
        if (!empty($this->senderNumber)) {
            return $this->senderNumber;
        }
        if (empty($defaultSenderNumber = $this->getDefaultSenderNumber())) {
            throw new SmsGatewayException(SmsGatewayException::PROVIDER_DONT_HAVE_DEFAULT_NUMBER);
        }

        return $defaultSenderNumber;
    }

    /**
     * @return string
     * @throws SmsGatewayException
     */
    public function getMessage(): string
    {
        if (empty($this->message)) {
            throw new SmsGatewayException(SmsGatewayException::EMPTY_MESSAGE);
        }

        return $this->message;
    }

    /**
     * @return array
     * @throws SmsGatewayException
     */
    public function getRecipients(): array
    {
        if (empty($this->recipients)) {
            throw new SmsGatewayException(SmsGatewayException::DEFINED_RECEPTOR_FIRST);
        }

        return $this->recipients;
    }
}
