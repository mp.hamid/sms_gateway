<?php
if (!function_exists('toCamelCase')) {
    /**
     * @param $string
     * @param array $noStrip
     * @return string
     */
    function toCamelCase($string, array $noStrip = []): string
    {
        // non-alpha and non-numeric characters become spaces
        $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $string);
        $str = trim($str);
        // uppercase the first character of each word
        $str = ucwords($str);
        $str = str_replace(" ", "", $str);
        $str = lcfirst($str);

        return $str;
    }
}
if (!function_exists('checkMobileNumber')) {
    /**
     * @param $receptor
     * @return bool
     */
    function checkMobileNumber($receptor): bool
    {
        return (bool)preg_match('/^09[0-9]{9}$/', $receptor);
    }
}
