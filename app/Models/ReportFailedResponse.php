<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportFailedResponse extends Model
{
    protected $fillable = ['report_id', 'report_status_id', 'response'];
    const UPDATED_AT = null;
}
