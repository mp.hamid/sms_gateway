<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Report extends Model
{
    protected $fillable = ['provider', 'sender', 'recipients', 'message'];
    protected $casts = ['recipients' => 'json'];
    const UPDATED_AT = null;

    public function status(): HasMany
    {
        return $this->hasMany(ReportStatus::class);
    }
}
