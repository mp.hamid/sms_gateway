<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ReportStatus extends Model
{
    protected $fillable = ['report_id', 'status'];
    public const STATUS_QUEUED = 'queued';
    public const STATUS_SENDING = 'sending';
    public const STATUS_SUCCESS = 'success';
    public const STATUS_FAILED = 'failed';
    const UPDATED_AT = null;

    public function response(): HasOne
    {
        return $this->hasOne(ReportFailedResponse::class);
    }
}
