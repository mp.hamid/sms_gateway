<?php

namespace App\Exceptions;


class SmsGatewayException extends \Exception
{
    public const WRONG_PROVIDER_NAME = 'Incorrect provider name';
    public const CALL_UNDEFINED_METHOD = 'There is no called method';
    public const INVALID_MOBILE_NUMBER = 'SMS recipient number is incorrect';
    public const PROVIDER_DONT_HAVE_DEFAULT_NUMBER = 'Sms provider does not have a default number';
    public const DEFINED_RECEPTOR_FIRST = 'To send an SMS, first enter the recipient of the SMS';
    public const EMPTY_MESSAGE = 'SMS text could not be blank';
}
