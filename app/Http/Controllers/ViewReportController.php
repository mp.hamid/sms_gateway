<?php

namespace App\Http\Controllers;

use App\Models\Report;

class ViewReportController extends Controller
{
    public function index()
    {
        $query = Report::query()->with('status.response')
            ->orderBy('created_at', 'desc');
        $viewArray['reports'] = $query->paginate(20);
        return view('report', $viewArray);
    }
}
