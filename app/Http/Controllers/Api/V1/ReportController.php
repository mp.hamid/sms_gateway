<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReportResource;
use App\Models\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function report()
    {
        $reportRows = Report::query()->with('status.response')
            ->orderBy('created_at', 'desc')->paginate(20);
        return ReportResource::collection($reportRows);
    }
}
