<?php

namespace App\Http\Controllers\Api\V1;

use App\Exceptions\SmsGatewayException;
use App\SmsSevice\SmsGateway;
use App\Http\Controllers\Controller;
use App\Http\Requests\SendRequest;
use App\Jobs\SendSms;

class SendController extends Controller
{
    public function send(SendRequest $request)
    {
        try {
            $smsGateway = new SmsGateway();
            if (isset($request->provider)) {
                $smsGateway->setProvider($request->provider);
            }
            if (isset($request->sender_number)) {
                $smsGateway->setSenderNumber($request->sender_number);
            }
            $smsGateway->setReceptor($request->receptor_number)->setMessage($request->message);
            SendSms::dispatch($smsGateway);
            return response()->json(['message'=>'success']);
        } catch (SmsGatewayException $smsGatewayException) {
            return response()->json(['error'=>['code'=>'sms_error','message'=>$smsGatewayException->getMessage()]],422);
        }
    }
}
