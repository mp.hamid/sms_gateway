<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class ReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "provider" => $this->provider,
            "sender" => $this->sender,
            "recipients" => $this->recipients,
            "message" => $this->message,
            "created_at" => $this->created_at,
            "statuses" => StatusResource::collection($this->status),
        ];
    }
}
