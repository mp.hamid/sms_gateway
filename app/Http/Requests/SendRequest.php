<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed $provider
 * @property mixed $sender_number
 * @property mixed $receptor_number
 * @property mixed $message
 */
class sendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider' => 'string',
            'sender_number' => 'string',
            'receptor_number' => 'required|string',
            'message' => 'required|string',
        ];
    }
}
