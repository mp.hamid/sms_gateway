<?php

namespace App\Jobs;

use App\Exceptions\SmsGatewayException;
use App\SmsSevice\SmsGateway;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private SmsGateway $smsGateway;

    /**
     * Create a new job instance.
     *
     * @return void
     * @throws SmsGatewayException
     */
    public function __construct(SmsGateway $smsGateway)
    {
        $this->smsGateway = $smsGateway;
        $this->smsGateway->storeReport();
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws SmsGatewayException
     */
    public function handle()
    {
        $this->smsGateway->send();
    }
}
