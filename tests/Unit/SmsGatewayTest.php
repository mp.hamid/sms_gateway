<?php

namespace Tests\Unit;

use App\Exceptions\SmsGatewayException;
use App\SmsSevice\SmsGateway;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Tests\TestCase;

class SmsGatewayTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed');
    }

    public function testExceptionWrongProviderName()
    {
        $this->expectException(SmsGatewayException::class);
        $this->expectExceptionMessage(SmsGatewayException::WRONG_PROVIDER_NAME);
        $smsGateway = new SmsGateway();
        $smsGateway->setProvider('test');
    }

    public function testExceptionUndefinedMethod()
    {
        $this->expectException(SmsGatewayException::class);
        $this->expectExceptionMessage(SmsGatewayException::CALL_UNDEFINED_METHOD);
        $smsGateway = new SmsGateway();
        $smsGateway->setUndefinedVariable();
    }

    public function testExceptionInvalidMobileNumberInSingleRecipient()
    {
        $this->expectException(SmsGatewayException::class);
        $this->expectExceptionMessage(SmsGatewayException::INVALID_MOBILE_NUMBER);
        $smsGateway = new SmsGateway();
        $smsGateway->setProvider('magfa')->setReceptor('091252');
    }

    public function testExceptionInvalidMobileNumberInMultiRecipient()
    {
        $this->expectException(SmsGatewayException::class);
        $this->expectExceptionMessage(SmsGatewayException::INVALID_MOBILE_NUMBER);
        $smsGateway = new SmsGateway();
        $smsGateway->setProvider('magfa')->setRecipients(['09125221691', '091252']);
    }

    public function testExceptionProviderDontHaveDefaultNumber()
    {
        $this->expectException(SmsGatewayException::class);
        $this->expectExceptionMessage(SmsGatewayException::PROVIDER_DONT_HAVE_DEFAULT_NUMBER);
        $smsGateway = new SmsGateway();
        Config::set('sms.providers.magfa.default_number', '');
        $smsGateway->setProvider('magfa')->setReceptor('09125221691')->send();
    }

    public function testExceptionDefinedReceptorFirst()
    {
        $this->expectException(SmsGatewayException::class);
        $this->expectExceptionMessage(SmsGatewayException::DEFINED_RECEPTOR_FIRST);
        $smsGateway = new SmsGateway();
        $smsGateway->setProvider('magfa')->setMessage('test')->send();
    }

    public function testExceptionEmptyMessage()
    {
        $this->expectException(SmsGatewayException::class);
        $this->expectExceptionMessage(SmsGatewayException::EMPTY_MESSAGE);
        $smsGateway = new SmsGateway();
        $smsGateway->setProvider('magfa')->setReceptor('09125221691')->send();
    }

    public function testCreateReport()
    {
        $smsGateway = new SmsGateway();
        $providerName = 'magfa';
        $receptor = '09125221691';
        $message = 'test message';
        $smsGateway->setProvider($providerName)
            ->setReceptor($receptor)
            ->setMessage($message);
        $smsGateway->storeReport();
        $this->assertDatabaseHas('reports', [
            'provider' => $providerName,
            'sender' => \config('sms.providers.'.$providerName.'.default_number'),
            'recipients' => json_encode([$receptor]),
            'message' => $message
        ]);
    }
}
