<?php
return[
    'providers' => [
        'kavenegar' => [
            'api_key' => env('KAVENEGAR_API_KEY'),
            'default_number' => env('KAVENEGAR_DEFAULT_SENDER_NUMBER','100077')
        ],
        'magfa' => [
            'api_key' => base64_encode(env('MAGFA_USER_NAME') . ':' . env('MAGFA_PASSWORD')),
            'default_number' => env('MAGFA_DEFAULT_SENDER_NUMBER','+983000862100'),
        ],
    ],
    'default_provider' => env('DEFAULT_PROVIDER', 'Kavenegar'),
];
